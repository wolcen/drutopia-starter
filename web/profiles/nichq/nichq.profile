<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Field\WidgetBase;

/**
 * Implements hook_help().
 */
function nichq_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.nichq':
      $output = '<h2>' . t('@TODO: Add Editor\'s guide') . '</h2>';

      return $output;
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function nichq_field_widget_paragraphs_form_alter(&$element, \Drupal\Core\Form\FormStateInterface $form_state, $context) {
  /** @var FieldConfig $field_definition */
  $field_definition = $context['items']->getFieldDefinition();

  $paragraph_field_name = $field_definition->getName();

  if ($paragraph_field_name == 'field_sections') {
    /** @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::formElement() */
    $widget_state = WidgetBase::getWidgetState($element['#field_parents'], $paragraph_field_name, $form_state);
    // Determine which paragraph type is being embedded.
    $paragraph_type = $widget_state['paragraphs'][$element['#delta']]['entity']->bundle();

    if ($paragraph_type == 'embedded_image_or_video') {
      $dependee_field_name = 'field_image_or_video';
      $selector = sprintf('select[name="%s[%d][subform][%s]"]', $paragraph_field_name, $element['#delta'], $dependee_field_name);

      // Dependent fields.
      $element['subform']['field_image']['#states'] = array(
        'visible' => array(
          $selector => array('value' => 'Image'),
        ),
      );

      $element['subform']['field_video']['#states'] = array(
        'visible' => array(
          $selector => array('value' => 'Video'),
        ),
      );
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function nichq_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if ($form_id == 'node_publication_form' || 
      $form_id != 'node_publication_edit_form') {

    $form['field_link']['#states'] = [
      'visible' => [
        ':input[name="field_source"]' => ['value' => 'Link'],
      ],
    ];

    $form['field_file']['#states'] = [
      'visible' => [
        ':input[name="field_source"]' => ['value' => 'File'],
      ],
    ];
    
  }
}

/**
 * Import a piece of content exported by default content module.
 */
function nichq_import_default_content($path_to_content_json) {
  list($entity_type_id, $filename) = explode('/', $path_to_content_json);
  $p = drupal_get_path('profile', 'nichq');
  $encoded_content = file_get_contents($p . '/content/' . $path_to_content_json);
  $serializer = \Drupal::service('serializer');
  $content = $serializer->decode($encoded_content, 'hal_json');
  global $base_url;
  $url = $base_url . base_path();
  $content['_links']['type']['href'] = str_replace('http://drupal.org/', $url, $content['_links']['type']['href']);
  $contents = $serializer->encode($content, 'hal_json');
  // The path for the taxonomy term class is different.
  if ($entity_type_id == 'taxonomy_term') {
    $class = 'Drupal\taxonomy\Entity\Term';
  } else {
    $class = 'Drupal\\' . $entity_type_id . '\Entity\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', $entity_type_id)));
  }
  $entity = $serializer->deserialize($contents, $class, 'hal_json', array('request_method' => 'POST'));
  $entity->enforceIsNew(TRUE);
  $entity->save();
}
