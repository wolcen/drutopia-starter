<?php

$aliases['dev'] = array(
  'root' => '/var/www/drupal/web',
  'uri' => 'http://dev.nichq.org',
  'remote-user' => 'root',
  'remote-host' => 'dev.nichq.org',
  'path-aliases' => array(
    '%drush' => '/var/www/drupal/vendor/drush',
    '%drush-script' => '/var/www/drupal/vendor/bin/drush',
  ),
);

$aliases['production'] = array(
  'root' => '/var/www/drupal/web',
  'uri' => 'https://www.nichq.org',
  'remote-user' => 'root',
  'remote-host' => 'www.nichq.org',
  'path-aliases' => array(
    '%drush' => '/var/www/drupal/vendor/drush',
    '%drush-script' => '/var/www/drupal/vendor/bin/drush',
  ),
);

?>
