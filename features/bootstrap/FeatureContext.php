<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Exception\ElementNotFoundException;
use Drupal\DrupalExtension\Context\DrupalContext;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * Gives us acess to the other contexts so we can access their properties.
   *
   * @BeforeScenario
   */
  public function gatherContexts(BeforeScenarioScope $scope) {
    $environment = $scope->getEnvironment();
    $this->contexts['drupal'] = $environment->getContext('Drupal\DrupalExtension\Context\DrupalContext');
    $this->contexts['mink'] = $environment->getContext('Drupal\DrupalExtension\Context\MinkContext');
  }

  /**
   * @When I select the first autocomplete option for :prefix on the :field field
   */
  public function iSelectFirstAutocomplete($prefix, $field) {
    $session = $this->getSession();
    $element = $session->getPage()->findField($field);
    if (empty($element)) {
      throw new ElementNotFoundException($session, NULL, 'named', $field);
    }
    $element->setValue($prefix);
    $element->focus();
    $xpath = $element->getXpath();
    $driver = $session->getDriver();
    // autocomplete.js uses key down/up events directly.
    // Press the down arrow to open the autocomplete options.
    $driver->keyDown($xpath, 40);
    $driver->keyUp($xpath, 40);
    $this->contexts['mink']->iWaitForAjaxToFinish();
    // Select the first option.
    $driver->keyDown($xpath, 40);
    $driver->keyUp($xpath, 40);
    // Press the Enter key to confirm selection, copying the value into the field.
    $driver->keyDown($xpath, 13);
    $driver->keyUp($xpath, 13);
    $this->contexts['mink']->iWaitForAjaxToFinish();
  }

  /**
   * @When I fill in the autocomplete :autocomplete with :text and click :popup
   */
  public function fillInDrupalAutocomplete($field, $text, $popup) {
    $session = $this->getSession();
    $element = $session->getPage()->findField($field);
    if (empty($element)) {
      throw new ElementNotFoundException($session, NULL, 'named', $field);
    }
    $element->setValue($text);
    $element->focus();
    $xpath = $element->getXpath();
    $driver = $session->getDriver();
    // autocomplete.js uses key down/up events directly.
    // Press the down arrow to open the autocomplete options.
    $driver->keyDown($xpath, 40);
    $driver->keyUp($xpath, 40);
    $this->contexts['mink']->iWaitForAjaxToFinish();
    $available_autocompletes = $this->getSession()->getPage()->findAll('css', 'ul.ui-autocomplete[id^=ui-id]');
    if (empty($available_autocompletes)) {
      throw new \Exception(t('Could not find the autocomplete popup box'));
    }
    // It's possible for multiple autocompletes to be on the page at once,
    // but it shouldn't be possible for multiple to be visible/open at once.
    $matched_element = NULL;
    foreach ($available_autocompletes as $autocomplete) {
      if ($autocomplete->isVisible()) {
        $matched_element = $autocomplete->find('xpath', "//a[text()='${popup}']");
        if (!empty($matched_element)) {
          $matched_element->click();
          $this->contexts['mink']->iWaitForAjaxToFinish();
        }
      }
    }

    if (empty($matched_element)) {
      throw new \Exception(t('Could not find autocomplete popup text @popup', array(
        '@popup' => $popup)));
    }
  }

  /**
   * @Given /^I wait (\d+) seconds$/
   */
  public function iWaitSeconds($seconds)
  {
    sleep($seconds);
  }


  /** @AfterFeature */
  public static function teardownFeature($event)
  {
    $email = "behat-test@agaric.com";

    // Delete any test user.
    if ($user = user_load_by_mail($email)) {
      user_delete($user->id());
    }
  }

}
