[README.md](README.md)

# Paragraph Types

1. [Long Text](#long-text)
1. [Full-Width Slideshow](#full-width-slideshow)
1. [Content-Width Slideshow](#content-width-slideshow)
1. [Slide](#slide)
1. [Quotation](#quotation)
1. [Image](#image)
1. [Video](#video)
1. [Highlight Grid](#highlight-grid)
1. [Carousel](#carousel)


// I've proposed using nested paragraphs for handling the "Full-Width Slideshow" and "Content-Width Slideshow" paragraph types&mdash;that way each image can have an associated text snippet that can be overlaid. *MAURICIO SAYS A-OK.*

// "Full-Width Slideshows" are intended for use just with "Campaigns", whereas the "Content-Width Slideshows" can be used in other content types as well. *AYE-AYE!*

// The "Image" paragraph type allows for left, right, and full width positioning. If left or right, then text can also be added. Also, I'm guessing that we'll probably just style these differently if used in a "Campaign" node versus other types. *MATT NOT SURE WHAT IN THE WORLD HE WAS TALKING ABOUT HERE WITH ALL THIS LEFT, RIGHT NONSENSE.*

// Not sure if the "Carousel" will work as a generic paragraph type to handle rotating teasers of more than just a single content type. In other words, in the wireframes we have an "Insights" carousel and one for "Testimonials". But those get displayed differently. They way I have this set up currently is that each item gets styles based on its type&mdash;not based on which carousel it's in. Not sure if that's desireable... *ACTUALLY, I'VE ADDED AN "ITEM COUNT" FIELD SO THAT WE CAN JUST PICK HOW MANY TO DISPLAY. NOT SURE IF THAT'S THE BEST WAY, EITHER... MAURICIO?*


# <a name="long-text"></a>Long Text

### *Fields*
- **Header** (text)
- **Body** (long text, no teaser)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study
- **Content Type**: Action


# <a name="full-width-slideshow"></a>Full-Width Slideshow

### *Fields*
- **Slide** (paragraph; "Slide")
- **Automatically Rotate** (boolean; "Yes", "No")

### *Used By*
- **Content Type**: Campaign


# <a name="content-width-slideshow"></a>Content-Width Slideshow

### *Fields*
- **Slide** (paragraph; "Slide")
- **Automatically Rotate** (boolean; "Yes", "No")

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study


# <a name="slide"></a>Slide

### *Fields*
- **Image** (image)
- **Header** (text)
- **Text** (long text, no teaser)
- **Link** (link)

### *Used By*
- Paragraph: Full-Width Slideshow
- Paragraph: Content-Width Slideshow


# <a name="quotation"></a>Quotation

### *Fields*
- **Text** (long text, no teaser)
- **Attribution** (text)
- **Color** (text -or- color_field)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study


# <a name="image"></a>Image

### *Fields*
- **Image** (image)
- **Caption** (long text, no teaser)
- **Position** (reference, taxonomy; "Position")
- **Text** (long text, no teaser)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study


# <a name="video"></a>Video

### *Fields*
- **Video** (video embed)
- **Caption** (long text, no teaser)
- **Position** (reference, taxonomy; "Position")
- **Text** (long text, no teaser)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study


# <a name="highlight-grid"></a>Highlight Grid

### *Fields*
- **Title**
- **Body** (long text, no teaser)
- **Image** (reference, image)
- **Link** (link)
- **Columns** (number, integer)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study


# <a name="carousel"></a>Carousel

### *Fields*
- **Card** (reference, content)
- **Items Count** (number, integer)

### *Used By*
- **Content Type**: Page
- **Content Type**: Campaign
- **Content Type**: Insight
- **Content Type**: Case Study

