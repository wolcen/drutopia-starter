# Getting started

```
git clone --recursive git@gitlab.com:nichq/nichq-d8-rebuild.git
cd nichq-d8-rebuild/
vagrant up
```

Note the `--recursive` which is required as we are using git submodules for the virtual machine.

# Theme development

The theme for NICHQ.org is named scott, in honor of Scott Dynes who
brought Agaric, and so Todd, into the NICHQ orbit.  But if he asks you
can totally tell NICHQ's CEO it's named after him.

## Updating the styleguide

This theme uses the NICHQ styleguide.  If you need to make changes to the
styleguide, it is done in a separate repository.  If you'd like to work
on it, or simply bring updates someone else has made to it into the
theme, here's the steps:

```
cd web/themes/scott/sass/
rm -rf nichq-styleguide
git clone git@gitlab.com:nichq/nichq-styleguide.git
```

Now you can `git add -p` your changes— from the project repository to
commit updates made to the styleguide, and from within nichq-styleguide
to make changes to the styleguide itself.

Note that changes to the NICHQ styleguide will need to work across all sites.
Don't do it unless you know what you're doing.

## Overriding the styleguide

This isn't expected to be needed much, but site-specific overrides to
the NICHQ styleguide should be done in
`web/themes/scott/sass/_specific.scss`

## Generating CSS from Sass

To see the theme in its glory locally, you'll need to turn the CSS into
Sass.

[Install Sass](http://sass-lang.com/install) (if not already installed);
alternatively you can also use [node-sass](https://github.com/sass/node-sass).

The virtual machine is provisioned with the sassc compiler. If compiling
with this command, use the executable sassc in place of sass below.

Compile the Sass to CSS with:

```
sass web/themes/scott/sass/styles.scss web/themes/scott/css/styles.css
```

Alternatively, you can autocompile on all changes to scss files with:

```
sass --watch web/themes/scott/sass/styles.scss:web/themes/scott/css/styles.css
```

## Default Content ##

On site install, some taxonomy terms and other content (privacy policy, etc.) will be read in from the json files in the profiles/nichq/content folder, which were produced using the default content module. Refer to Agaric's [sample code](http://data.agaric.com/drupal-8-default-content-agaric-way) for examples of how to create default content using drush.
